import { combineReducers } from "redux";
import useReducer  from "./user";
import hobbyReducer from "./hobby";

const rootReducer = combineReducers({
    hobby: hobbyReducer,
    user: useReducer
})
export default rootReducer;