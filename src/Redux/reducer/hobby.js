
const State = {
    soluong:0
}

const hobbyReducer = (state = State, action) =>{
    switch (action.type) {
        case 'ADD_HOBBY':{
            const newSoluong = action.payload;
            
            return {
                soluong : newSoluong
            }
        }
    
        case 'SET_ACTIVE_HOBBY':{
            return state
        }
    
        default:
            return state;
    }
}
export default hobbyReducer;