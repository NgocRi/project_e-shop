import axios from "axios";
import { useEffect, useState } from "react";
import Errors from "../Errors/Errors";

function Account(props){
   
    
    const [update, setUpdate] = useState({
        name:"",
        email:"",
        password:"",
        phone:"",
        address:""
    });
    
    useEffect(()=>{
        let checkUser = localStorage.getItem("user")
        if(checkUser){
            checkUser = JSON.parse(checkUser);
                setUpdate({
                email : checkUser.data.Auth.email,
                name : checkUser.data.Auth.name,
                phone : checkUser.data.Auth.phone,
                address : checkUser.data.Auth.address,
            })
        }
    },[]);
    
    const [files, setFile] = useState("");
    const [avatar, setAvatar] = useState("");
    const arrTypeImage = ["png", "jpg", "jpeg", "PNG", "JPG"];
    const [err, setErr] = useState({});
    

    const handelInput=(e)=>{
        const nameInput = e.target.name;
        const valueInpust = e.target.value;

        setUpdate(state=>({...state,[nameInput]:valueInpust}));
    }

    function handelFile(e){
        const file = e.target.files;

        let reader = new FileReader();
        reader.onload = (e) =>{
            setAvatar(e.target.result);
            setFile(file[0]);
        };
        reader.readAsDataURL(file[0]);
    }

    // kiểm tra định dạng mail
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    function handelSubmit(e){
        e.preventDefault();
        let errSubmit = {};
        let flag = true;
        if(update.name.trim() == "") {
            errSubmit.name = "Vui lòng nhập Full Name:"
            flag = false;
        }

        if(update.email.trim() == ""){
            errSubmit.email = "Vui lòng nhập Email:"
            flag = false;
        }else if(!isEmail(update.email)){
            errSubmit.email = "Email bạn nhập không đúng định dạng:"
            flag = false;
        }

        if(update.phone.trim() ==""){
            errSubmit.phone = "Vui lòng nhập Phone:"
            flag = false;
        }

        if(update.address.trim() ==""){
            errSubmit.address = "Vui lòng nhập Address:"
            flag = false;
        }

        if(files == ""){
            errSubmit.files = "Vui lòng chọn File:"
            flag = false;
        }else{
            const fileType = files.type;
            const fileSize = files.size
            const TypeImg = fileType.split('/').pop();

            if(!arrTypeImage.includes(TypeImg)){
                errSubmit.files = "File bạn chọn không phải ảnh:" 
                flag = false;
            }else if(fileSize > 1024*1024){
                errSubmit.files = "File bạn chọn dung lượng quá 1gb:"
            }
        }

        if(!flag){
            setErr(errSubmit);
        }else{
            setErr("");
           
            if(flag){
                let checkUser = localStorage.getItem("user")
                checkUser = JSON.parse(checkUser);

                const accessToken = checkUser.data.success.token;
                const id_user =  checkUser.data.Auth.id;
                const config = { 
                    headers: { 
                    'Authorization': 'Bearer '+ accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                    } 
                    };
                const data = {
                    name: update.name,
                    email: update.email,
                    password: update.password?update.password:"",
                    phone: update.phone,
                    address: update.address,
                    avatar: avatar,
                    level:0
                }
                // console.log(accessToken);
                axios.post("http://demo-laravel:8888/api/user/update/"+ id_user,data,config)
                .then(response => {
                    console.log(response);

                    if(response.data.errors){
                        setErr(response.data.errors);
                    }else{
                        alert("Thành công:")
                    }
                })
            }
        }
        
    }
    
    // console.log(email);
    return(
        <div className="col-sm-9">
                <h3>Update</h3>
                <form  onSubmit={handelSubmit}>
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Full Name</label>

                    <div class="col-md-8">
                        <input id="name" type="text"  name="name" value={update.name} onChange={handelInput} />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Email </label>

                    <div class="col-md-8">
                        <input id="email" type="text" name="email" value={update.email} onChange={handelInput} readOnly/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Password</label>

                    <div class="col-md-8">
                        <input id="password" type="password" name="password" onChange={handelInput}/>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Phone</label>

                    <div class="col-md-8">
                        <input id="phone" type="text"  name="phone" value={update.phone} onChange={handelInput}/>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Address</label>

                    <div class="col-md-8">
                        <input id="address"  value={update.address}  name="address"  onChange={handelInput} type="text"/>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Avatar</label>

                    <div class="col-md-8">
                        <input id="avatar" type="file" name="avatar" onChange={handelFile}/>
                    </div>
                </div>

                <div class="form-group row mb-0">
                        <button type="submit" >Register</button>
                </div>
                </form>
                <Errors err = {err}/>
       </div>
    )
}
export default Account;