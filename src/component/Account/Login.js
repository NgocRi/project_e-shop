import axios from "axios";
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Errors from "../Errors/Errors";
import Header from "../layout/Header";

function Login(proos){
    const [login, setLogin] = useState({
        email:"",
        password:""
    });
    const [err, setErr] = useState({});
    const hadelInput = (e) =>{
        const nameInput = e.target.name;
        const valueInput = e.target.value;

        setLogin(state =>({...state,[nameInput]:valueInput}));
    };

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    };

    const navigate = useNavigate();
    function handelSubmit(e){
        e.preventDefault();
        let flag = true;
        let errSubmit = {};
        if(login.email.trim() == ""){
            errSubmit.email = "Vui lòng nhập Email:"
            flag = false;
        }else if(!isEmail(login.email)){
            errSubmit.email = "Email bạn vừa nhập không đúng định dạng:"
            flag = false;
        }

        if(login.password.trim() == ""){
            errSubmit.password = "Vui lòng nhập Password"
            flag = false;
        }

        if(!flag){
            setErr(errSubmit);
        }else{
            setErr("");
            if(flag){
                const data = {
                    email : login.email,
                    password : login.password,
                    level : 0
                }
                axios.post("http://demo-laravel:8888/api/login",data)
                .then(response => {
                    console.log(response);
                    localStorage.setItem(("user"),JSON.stringify(response));
                    if(response.data.errors){
                        setErr(response.data.errors)
                        navigate('/Login')
                    }else{
                        alert("thành công");
                        navigate('/');
                    }
                })
                   const flagLogin = true
                
                localStorage.setItem(("check"),JSON.stringify(flagLogin));
            }
        }
    };
    return(
        
            <section  className="col-sm-9 login" >
                <form  onSubmit={handelSubmit}>
                    {/* <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">Email</label>

                        <div class="col-md-8">
                            <input id="email" type="text"   name="email" onChange={hadelInput}/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">Password</label>

                        <div class="col-md-8">
                            <input id="password" type="password"  name="password" onChange={hadelInput}/>
                        </div>
                    </div> */}
                     <div class=" form-group row mb-8">
                        <label for="inputEmail3" class="col-sm-2 col-form-label" >Email</label>
                        <div class="col-sm-10">
                        <input id="email" type="text"   name="email" onChange={hadelInput}/>
                        </div>
                    </div>
                    <div class="form-group row mb-8">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                        <input id="password" type="password"  name="password" onChange={hadelInput}/>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Sign in</button>
                </form>

                <Errors err = {err}/>
               
	        </section>
       
    )
}
export default Login;