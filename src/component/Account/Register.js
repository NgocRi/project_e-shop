import axios from "axios";
import { useEffect, useState } from "react";
import Errors from "../Errors/Errors";

function Register(props){
    const [register, setRegister] = useState({
        name:"",
        email:"",
        password:"",
        phone:"",
        address:""
    });
    const [files, setFile] = useState("");
    const [avatar, setAvatar] = useState("");
    const arrTypeImage = ["png", "jpg", "jpeg", "PNG", "JPG"];
    const [err, setErr] = useState({});
    

    const handelInput=(e)=>{
        const nameInput = e.target.name;
        const valueInpust = e.target.value;

        setRegister(state=>({...state,[nameInput]:valueInpust}));
    }

    function handelFile(e){
        const file = e.target.files;

        let reader = new FileReader();
        reader.onload = (e) =>{
            setAvatar(e.target.result);
            setFile(file[0]);
        };
        reader.readAsDataURL(file[0]);
    }

    // kiểm tra định dạng mail
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    function handelSubmit(e){
        e.preventDefault();
        let errSubmit = {};
        let flag = true;
        if(register.name.trim() == "") {
            errSubmit.name = "Vui lòng nhập Full Name:"
            flag = false;
        }

        if(register.email.trim() == ""){
            errSubmit.email = "Vui lòng nhập Email:"
            flag = false;
        }else if(!isEmail(register.email)){
            errSubmit.email = "Email bạn nhập không đúng định dạng:"
            flag = false;
        }

        if(register.password.trim() == ""){
            errSubmit.password = "Vui lòng nhập Password:"
            flag = false;
        }

        if(register.phone.trim() ==""){
            errSubmit.phone = "Vui lòng nhập Phone:"
            flag = false;
        }

        if(register.address.trim() ==""){
            errSubmit.address = "Vui lòng nhập Address:"
            flag = false;
        }

        if(files == ""){
            errSubmit.files = "Vui lòng chọn File:"
            flag = false;
        }else{
            const fileType = files.type;
            const fileSize = files.size
            const TypeImg = fileType.split('/').pop();

            if(!arrTypeImage.includes(TypeImg)){
                errSubmit.files = "File bạn chọn không phải ảnh:" 
                flag = false;
            }else if(fileSize > 1024*1024){
                errSubmit.files = "File bạn chọn dung lượng quá 1gb:"
            }
        }

        if(!flag){
            setErr(errSubmit);
        }else{
            setErr("");
           
            if(flag){
                const data = {
                    name: register.name,
                    email: register.email,
                    password: register.password,
                    phone: register.phone,
                    address: register.address,
                    avatar: avatar,
                    level:0
                }
                axios.post("http://demo-laravel:8888/api/register",data)
                .then(response => {
                    console.log(response);

                    if(response.data.errors){
                        setErr(response.data.errors);
                    }else{
                        alert("Thành công:")
                    }
                })
            }
        }
    }
    return(
        <div className="col-sm-9">
                <h3>Register</h3>
                <form encType="multipart/form-data" onSubmit={handelSubmit}>
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Full Name</label>

                    <div class="col-md-8">
                        <input id="name" type="text"  name="name" onChange={handelInput}/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Email </label>

                    <div class="col-md-8">
                        <input id="email" type="text" name="email" onChange={handelInput} />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Password</label>

                    <div class="col-md-8">
                        <input id="password" type="password" name="password" onChange={handelInput}/>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Phone</label>

                    <div class="col-md-8">
                        <input id="phone" type="text"  name="phone" onChange={handelInput}/>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Address</label>

                    <div class="col-md-8">
                        <input id="address" type="text"  name="address" onChange={handelInput}/>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">Avatar</label>

                    <div class="col-md-8">
                        <input id="avatar" type="file" name="avatar" onChange={handelFile}/>
                    </div>
                </div>

                <div class="form-group row mb-0">
                        <button type="submit" >Register</button>
                </div>
                </form>
                <Errors err = {err}/>
       </div>
    )
}
export default Register;