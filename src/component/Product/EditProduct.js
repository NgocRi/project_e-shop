import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Errors from "../Errors/Errors";

function EditProduct(props){
    const params = useParams();
    const navigate = useNavigate()
    const [editProduct, setEditProduct] = useState({
        name:"",
        price:"",
        company:"",
        detail:""
    })
    const [getcategory, setGetCategory] = useState({});
    const [getbrand, setGetBrand] = useState({});
    const [category, setCategory] = useState("");
    const [brand, setBrand] = useState("");
    const [status, setStatus] = useState("")
    const [sale, setSale] = useState("")
    const [img, setImg] = useState("")
    const [id_Product, setId_Product] = useState("");
    const arrTypeImage = ["png", "jpg", "jpeg", "PNG", "JPG"];
    const [err, setErr] = useState({})
    let checkUser = localStorage.getItem("user");
    checkUser = JSON.parse(checkUser);
    const accessToken = checkUser.data.success.token;

            let config = { 
                headers: { 
                'Authorization': 'Bearer '+ accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
                } 
            };

    useEffect(()=>{
        axios.get("http://demo-laravel:8888/api/category-brand")
        .then(res => {
            setGetCategory(res.data.category);
            setGetBrand(res.data.brand);
            // console.log(res);
        })
        &&
        axios.get("http://demo-laravel:8888/api/user/product/" + params.id,config)
        .then(res =>{
            // console.log(res);
            
            setEditProduct({
                name:res.data.data.name,
                price:res.data.data.price,
                company:res.data.data.company_profile,
                detail:res.data.data.detail
            })
            setCategory(res.data.data.id_category)
            setBrand(res.data.data.id_brand)
            setStatus(res.data.data.status)
            setSale(res.data.data.sale)
            setImg(res.data.data.image)

            setId_Product(res.data.data.id)
        })

    },[])
        // console.log(img);

    
    function handelInput(e){
        const nameInput = e.target.name;
        const valueInput = e.target.value;

        setEditProduct(state =>({...state,[nameInput]:valueInput}));
    }


    const [avatarCheckBox, setavatarCheckBox] = useState("");
    function handelCheckBox(e){
        const nameInput = e.target.name;
        const valueInput = e.target.value;
        const isChecked = e.target.checked;
        if(isChecked){
            setavatarCheckBox(state => [...state,valueInput] )
        }else if(isChecked == false){
            const removeIndex = avatarCheckBox.findIndex((item) => item === valueInput);
            avatarCheckBox.splice(removeIndex, 1);
            
        }
    }
    // console.log(avatarCheckBox);

    const [imgEdit,setImgEdit]= useState("")
    function handelFile(e){
        setImgEdit(e.target.files)
    }
    // console.log(imgEdit);
    
    function handelCategory(e){
        setCategory(e.target.value)
    }
    // console.log(category);
    function handelBrand(e){
        setBrand(e.target.value)
    }
    // console.log(brand);
    
    function handelStatus(e){
       setStatus(e.target.value)
    }
    function handelSale(e){
            setSale(e.target.value)
    }
    
    function readerCategory(){
        if(getcategory.length > 0){
            return getcategory.map((value,key)=>{
                return(
                        <option key={key} value={value.id} >{value.category}</option>
                )
            })
        }
    }

    function readerBrand(){
        if(getbrand.length > 0){
            return getbrand.map((value2,key)=>{
                return(
                        <option key={key} value={value2.id} >{value2.brand}</option>
                )
            })
        }
    }

    function rendersale (){
        if(status == 1){
            return(
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">sale </label>

                    <div class="col-md-8">
                        <p><input id="sale" type="text" name="sale" value={sale?sale:0}  onChange={handelSale}/>%</p>
                    </div>
                </div>
            )
        }else if(status == 0){
            return ("")
        }
    }
    
    function renderImg(e){
        if(img.length >0 ){
            return  img.map((value, key)=>{
                const srcImg = "http://demo-laravel:8888/upload/user/product/" + checkUser.data.Auth.id + "/" + value ;
                return (
                    
                        <div key={key} className="col-md-2">
                            <img width="100px" height="100px"src={srcImg}/>
                            <div>
                            <input name="avatarCheck" type="checkbox" value={value} onChange={handelCheckBox} />
                            </div>
                        </div>
                )
            })
        }
        
    }
    
    
    function handelSubmit(e) {
        e.preventDefault();
        let flag = true;
        let errSubmit = {};
        if(editProduct.name == ""){
            errSubmit.name = "vui lòng nhập name";
            flag = false;
        }
        if(editProduct.price == ""){
            errSubmit.price = "Vui lòng nhập price";
            flag = false;
        }
        if(category == ""){
            errSubmit.category = "vui lòng chọn category";
            flag = false;
        }
        if(brand ==""){
            errSubmit.brand = "Vui lòng chọn brand";
            flag = false;
        }
        if(editProduct.company == ""){
            errSubmit.company = "vui lòng nhập company";
            flag = false;
        }
        if(status === ""){
            errSubmit.status = "Vui lòng chọn status";
            flag = false;
        }else if(status == 1){
            if(sale == ""){
                errSubmit.sale = "Vui lòng nhập sale";
                flag = false;
            }
        }
        
        if(imgEdit == ""){
            errSubmit.img = "Vui lòng chọn file ảnh";
            flag = false;
        }else{
            if(Object.keys(imgEdit).length > 3){
                errSubmit.img = "Không được vượt quá 3 file ảnh";
                flag = false;
            }else{
                Object.keys(imgEdit).map((key,index)=>{
                    const typeFile = imgEdit[key].type
                    const sizeFile = imgEdit[key].size
                    const TypeImg = typeFile.split('/').pop();
                    // console.log(TypeImg);
                    
                    if(!arrTypeImage.includes(TypeImg)){
                        errSubmit.img = "file bạn chọn không phải ảnh" + typeFile;
                        flag = false;
                        // console.log("xxxx" + typeFile);
                    }else{
                        if(sizeFile > (1024 * 1024)){
                            errSubmit.img = "file bạn chọn dung lượng quá 1gb";
                            flag = false;
                        }
                    }
                })
            }
            
        }
        if(editProduct.detail == ""){
            errSubmit.detail = "Vui lòng nhập detail";
            flag = false;
        }

        if(!flag){
            setErr(errSubmit);
        }else{
            if(!checkUser){
                alert("Vui lòng đăng nhập");
            }else{
                
                let config = { 
                        headers: { 
                        'Authorization': 'Bearer '+ accessToken,
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Accept': 'application/json'
                        } 
                    };
                
                let formData = new FormData();
                formData.append('name',editProduct.name);
                formData.append('price',editProduct.price);
                formData.append('category',category);
                formData.append('brand',brand);
                formData.append('company',editProduct.company);
                formData.append('detail',editProduct.detail);
                formData.append('status',status);
                formData.append('sale',status==1?sale:"");
                Object.keys(imgEdit).map((item, i)=>{
                    formData.append('file[]',imgEdit[item])
                })
                Object.keys(avatarCheckBox).map((item2, i)=>{
                    formData.append('avatarCheckBox[]',avatarCheckBox[item2])
                })

                 axios.post("http://demo-laravel:8888/api/user/edit-product/" + id_Product,formData,config)
                 .then(res =>{
                    if(res.data.errors){
                        console.log(res.data.errors);
                    }else{
                        console.log(res);
                        setErr("")
                        alert("thanh công")
                        navigate('/MyProduct')
                    }
                 })
            }
        }

    }
    return(
        <div className="col-sm-9">
            <h4>Edit Product</h4>
            <form onSubmit={handelSubmit}>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">Name </label>

                    <div className="col-md-8">
                        <input id="name" type="text" name="name" value={editProduct.name} onChange={handelInput} />
                    </div>
            </div>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">Price </label>

                    <div className="col-md-8">
                        <input id="price" type="text" name="price" value={editProduct.price} onChange={handelInput} />
                    </div>
            </div>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">Please choose category</label>

                    <div className="col-md-8">
                    <select id="category" type="text" name="category" value={category} onChange={handelCategory}>
                        <option value="">Chọn category</option>
                        {readerCategory()}
                    </select>
                    </div>
            </div>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">Please choose brand</label>

                    <div className="col-md-8">
                        <select id="brand" type="text" name="brand" value={brand} onChange={handelBrand}>
                            <option value="">Chọn brand</option>
                            {readerBrand()}
                        </select>
                    </div>
            </div>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">status</label>

                    <div className="col-md-8">
                        <select id="status" type="text" name="status" value={status} onChange={handelStatus}>
                            <option value="">chọn</option>
                            <option value="0">New</option>
                            <option value="1">Sale</option>
                        </select>
                    </div>
            </div>
            
            {rendersale()}

            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">company </label>

                    <div className="col-md-8">
                        <input id="company" type="text" name="company" value={editProduct.company}  onChange={handelInput}/>
                    </div>
            </div>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">Image </label>

                    <div className="col-md-8">
                        <input id="Image" type="file" name="Image"  multiple  onChange={handelFile}/>
                    </div>
            </div>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">Image2 </label>

                    
                    {renderImg()}
            </div>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">detail </label>

                    <div className="col-md-8">
                        <textarea id="detail" type="file" name="detail" rows="11" value={editProduct.detail} onChange={handelInput}></textarea>
                    </div>
            </div>
            <button type="submit">Add Product</button>
            </form>
            <Errors err = {err}/>
        </div>
    )
}
export default EditProduct;