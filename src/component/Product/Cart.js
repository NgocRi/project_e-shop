import axios from "axios";
import {  useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Context } from "../../Context/UserContext";

function Cart(props){
   const [cart, setCart] = useState("");
   let value = useContext(Context);
            useEffect(()=>{
                let item = JSON.parse(localStorage.getItem("cart"));
                if(item){
                    
                        axios.post("http://demo-laravel:8888/api/product/cart",item)
                        .then(res =>{
                            if(res.data.errors){
                                console.log(res.data.errors);
                            }else {
                                console.log(res.data.data);
                                setCart(res.data.data)
                            }
                        });
                        
                }
            },[])
    let obj=JSON.parse(localStorage.getItem("cart"));




    
    const dispatch = useDispatch()
    



    // Tăng 
    function increaseCart(e){
        
        let qty = e.target.id
        Object.keys(obj).map((key, index)=>{
            if(cart[qty].id == key){
                obj[cart[qty].id] = obj[cart[qty].id] + 1;
            }
        });
        
        localStorage.setItem("cart",JSON.stringify(obj));
        // console.log(obj)
        axios.post("http://demo-laravel:8888/api/product/cart",obj)
                    .then(res =>{
                        if(res.data.errors){
                            console.log(res.data.errors);
                        }else {
                            // console.log(res.data.data);
                            setCart(res.data.data)
                        }
        });
    }
   
    // Giảm
    function reduceCart(e){
        let qty = e.target.id
        // var ret = obj[cart[qty].price].replace('$', '')
        Object.keys(obj).map((key, index)=>{
            if(cart[qty].id == key){
                obj[cart[qty].id] = obj[cart[qty].id] - 1;
            }
        });
        // console.log(cart[qty].price);
        
        if(obj[cart[qty].id] == 0){
            delete obj[cart[qty].id];
        }
        localStorage.setItem("cart",JSON.stringify(obj));
        // console.log(obj);
        axios.post("http://demo-laravel:8888/api/product/cart",obj)
                    .then(res =>{
                        if(res.data.errors){
                            console.log(res.data.errors);
                        }else {
                            // console.log(res.data.data);
                            setCart(res.data.data)
                        }
        });
        // console.log(qty);
    }

    // Xóa
    function deteleProduct(e){
        let index = e.target.id
        // console.log(cart[index].id);
        delete obj[cart[index].id];
        localStorage.setItem("cart",JSON.stringify(obj));
        axios.post("http://demo-laravel:8888/api/product/cart",obj)
                    .then(res =>{
                        if(res.data.errors){
                            console.log(res.data.errors);
                        }else {
                            // console.log(res.data.data);
                            setCart(res.data.data)
                        }
        });
        
       
    }
    
    // Show
    var S = 0;
    var SoLuong = 0
    function renderCart (){
        
        return Object.keys(cart).map((key, index)=>{
            const arr = JSON.parse(cart[key].image)
            const srcImg = "http://demo-laravel:8888/upload/user/product/" + cart[key].id_user + "/" + arr[0]
            // console.log(cart[key]);
             let qty = cart[key].qty;
             let price = cart[key].price;
             let total = price * qty;
             SoLuong = SoLuong + qty;
             S = S + Number(total);

            //  context
             value.setTongSoLuong(SoLuong)

            //  redux
            const newSoLuong = SoLuong
            dispatch({
                type: 'ADD_HOBBY',
                payload: newSoLuong
            })
            return (
                <tr>
                    <td className="cart_product">
                        <a href=""><img width="200px" src={srcImg} alt=""/></a>
                    </td>
                    <td className="cart_description">
                        <h4><a href="">{cart[key].name}</a></h4>
                        <p>Web ID: {cart[key].id}</p>
                    </td>
                    <td className="cart_price">
                        <p>$ {cart[key].price}</p>
                    </td>
                    <td className="cart_quantity">
                        <div className="cart_quantity_button">
                            <a className="cart_quantity_up" id={index} onClick={increaseCart}> + </a>
                            <input className="cart_quantity_input" type="text" name="quantity" value={cart[key].qty} autocomplete="off" size="2"/>
                            <a className="cart_quantity_down" id={index} onClick={reduceCart}> - </a>
                        </div>
                    </td>
                    <td className="cart_total">
                         <p className="cart_total_price" >$ {total}</p>
                    </td>
                    <td className="cart_delete" >
                        <a className="cart_quantity_delete" ><i className="fa fa-times" id={index} onClick={deteleProduct}></i></a>
                    </td>
                </tr>
            )
        })
    }
    
    return(
        <section  className="col-md-6 row " >
            <section id="cart_items">
                <div className="container">
                    <div className="breadcrumbs">
                        <ol className="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li className="active">Shopping Cart</li>
                        </ol>
                    </div>
                    <div className="table-responsive cart_info">
                        <table className="table table-condensed">
                            <thead>
                                <tr className="cart_menu">
                                    <td className="image">Item</td>
                                    <td className="description"></td>
                                    <td className="price">Price</td>
                                    <td className="quantity">Quantity</td>
                                    <td className="total">Total</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                               {renderCart()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </section> 

            <section id="do_action">
                <div className="container">
                    <div className="heading">
                        <h3>What would you like to do next?</h3>
                        <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
                    </div>
                    <div className="row">
                        <div className="col-sm-6">
                            <div className="chose_area">
                                <ul className="user_option">
                                    <li>
                                        <input type="checkbox"/>
                                        <label>Use Coupon Code</label>
                                    </li>
                                    <li>
                                        <input type="checkbox"/>
                                        <label>Use Gift Voucher</label>
                                    </li>
                                    <li>
                                        <input type="checkbox"/>
                                        <label>Estimate Shipping & Taxes</label>
                                    </li>
                                </ul>
                                <ul className="user_info">
                                    <li className="single_field">
                                        <label>Country:</label>
                                        <select>
                                            <option>United States</option>
                                            <option>Bangladesh</option>
                                            <option>UK</option>
                                            <option>India</option>
                                            <option>Pakistan</option>
                                            <option>Ucrane</option>
                                            <option>Canada</option>
                                            <option>Dubai</option>
                                        </select>
                                        
                                    </li>
                                    <li className="single_field">
                                        <label>Region / State:</label>
                                        <select>
                                            <option>Select</option>
                                            <option>Dhaka</option>
                                            <option>London</option>
                                            <option>Dillih</option>
                                            <option>Lahore</option>
                                            <option>Alaska</option>
                                            <option>Canada</option>
                                            <option>Dubai</option>
                                        </select>
                                    
                                    </li>
                                    <li className="single_field zip-field">
                                        <label>Zip Code:</label>
                                        <input type="text"/>
                                    </li>
                                </ul>
                                <a className="btn btn-default update" href="">Get Quotes</a>
                                <a className="btn btn-default check_out" href="">Continue</a>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="total_area">
                            <ul>
                                <li>Cart Sub Total <span>${S?S:0} </span></li>
                                <li>Eco Tax <span>$2</span></li>
                                <li>Shipping Cost <span>Free</span></li>
                                <li>Total <span>${S?S + 2:2}</span></li>
                            </ul>
                                    <a className="btn btn-default update" href="">Update</a>
                                    <a className="btn btn-default check_out" href="">Check Out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
       
    )
}
export default Cart;