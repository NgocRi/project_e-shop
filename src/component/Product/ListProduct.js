import axios from "axios";
import { useEffect, useState } from "react";

function ListProduct(){
    const [listProduct, setListProduct] = useState("");
    useEffect(()=>{
        axios.get("http://demo-laravel:8888/api/product/list")
        .then(res =>{
            console.log(res.data.data);
        })
    },[])
    return(
        <div className="col-sm-9 padding-right">
					<div className="features_items">
						<h2 className="title text-center">Features Items</h2>
						<div className="col-sm-4">
							<div className="product-image-wrapper">
								<div className="single-products">
									<div className="productinfo text-center">
										<img src="images/home/product1.jpg" alt=""/>
										<h2>$56</h2>
										<p>Easy Polo Black Edition</p>
										<a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart"></i>Add to cart</a>
									</div>
									<div className="product-overlay">
										<div className="overlay-content">
											<h2>$56</h2>
											<p>Easy Polo Black Edition</p>
											<a href="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart"></i>Add to cart</a>
										</div>
									</div>
								</div>
								<div className="choose">
									<ul className="nav nav-pills nav-justified">
										<li><a href=""><i className="fa fa-plus-square"></i>Add to wishlist</a></li>
										<li><a href=""><i className="fa fa-plus-square"></i>Add to compare</a></li>
									</ul>
								</div>
							</div>
						</div>
						
					</div>
						<ul className="pagination">
							<li className="active"><a href="">1</a></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><a href="">»</a></li>
						</ul>
				</div>
    )
}
export default ListProduct;