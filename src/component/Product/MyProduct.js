import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import DeleteProduct from "./DeleteProduct";

function MyProduct(){
    let checkUser = localStorage.getItem("user");
    const [myProduct, setMyProduct] = useState("");
    useEffect(()=>{
        axios.get("http://demo-laravel:8888/api/product")
        .then(res => {
            setMyProduct(res.data.data)
        })
    },[])
    // if (window.confirm("Delete the item?")) {
        
        // }
        // console.log(myProduct);
    function deleteProduct(data){
        setMyProduct(data)
    }
    function renderProduct(){
       
        checkUser = JSON.parse(checkUser)
       return Object.keys(myProduct).map((key, index)=>{
            const arr = JSON.parse(myProduct[key].image)
            const srcImg = "http://demo-laravel:8888/upload/user/product/" + myProduct[key].id_user + "/" + arr[0]
            console.log(arr[0]);
            console.log(key);
            if(checkUser.data.Auth.id == myProduct[key].id_user){
             return(
                <tr key={key}>
                    <th>{myProduct[key].id}</th>
                    <td>{myProduct[key].name}</td>
                    <td><img width="100px" height="90px" src={srcImg}/></td>
                    <td>$ {myProduct[key].price}</td>
                    <td><Link  to={'/EditProduct/'+myProduct[key].id}> Edit</Link> <DeleteProduct accessToken= {checkUser.data.success.token} id={myProduct[key].id} deleteProduct={deleteProduct}/></td>
                </tr>
                );
            }
        })
    }

    return(
        <div className="col-sm-9">
            <table className="table">
                <thead>
                    <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Img</th>
                    <th scope="col">Price</th>
                    <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {checkUser?renderProduct():""}
                </tbody>
            </table>
            <Link to="/AddProduct">Add</Link>
		</div>
    )
}
export default MyProduct;