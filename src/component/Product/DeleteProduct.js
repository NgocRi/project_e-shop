import axios from "axios";

function DeleteProduct(props){
    function Delete(){
        
        // let checkUser = localStorage.getItem("user");
        // checkUser = JSON.parse(checkUser)
        // const accessToken = checkUser.data.success.token;
        let config = { 
            headers: { 
            'Authorization': 'Bearer '+ props.accessToken,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
            } 
        };
        axios.get("http://demo-laravel:8888/api/user/delete-product/"+ props.id,config)
        .then(res =>{
            if(res.data.errors){
                console.log(res.data.errors);
            }else{
                props.deleteProduct(res.data.data);
                // console.log(res);
            }
        })
    }
    return(
        <button onClick={Delete}>Delete</button>
    )
}
export default DeleteProduct;