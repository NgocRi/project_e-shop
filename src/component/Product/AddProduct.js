import axios from "axios";
import { useEffect, useState } from "react";
import Errors from "../Errors/Errors";
import DeleteProduct from "./DeleteProduct";

function AddProduct(props){
    const [addProduct, setAddProduct] = useState({
        name:"",
        price:"",
        company:"",
        detail:""
    })
    const [getcategory, setGetCategory] = useState({});
    const [getbrand, setGetBrand] = useState({});
    const [category, setCategory] = useState("");
    const [brand, setBrand] = useState("");
    const [status, setStatus] = useState("")
    const [sale, setSale] = useState("")
    const [img, setImg] = useState("")
    const arrTypeImage = ["png", "jpg", "jpeg", "PNG", "JPG"];
    const [err, setErr] = useState({})
    useEffect(()=>{
        axios.get("http://demo-laravel:8888/api/category-brand")
        .then(res => {
            setGetCategory(res.data.category);
            setGetBrand(res.data.brand);
        })
    },[])
    function readerCategory(){
        if(getcategory.length > 0){
            return getcategory.map((value,key)=>{
                return(
                        <option value={value.id} >{value.category}</option>
                )
            })
        }
    }
    function readerBrand(){
        if(getbrand.length > 0){
            return getbrand.map((value2,key)=>{
                return(
                        <option value={value2.id} >{value2.brand}</option>
                )
            })
        }
    }
   
    function handelInput(e){
        const nameInput = e.target.name;
        const valueInput = e.target.value;

        setAddProduct(state =>({...state,[nameInput]:valueInput}));
    }
    function handelFile(e){
        setImg(e.target.files)
    }
    
    
    // console.log(img);
    function handelCategory(e){
        setCategory(e.target.value)
    }
    // console.log(category);
    function handelBrand(e){
        setBrand(e.target.value)
    }
    // console.log(brand);
    
    function handelStatus(e){
       setStatus(e.target.value)
    }
    function handelSale(e){
            setSale(e.target.value)
    }
    
    function rendersale (){
        if(status == 1){
            return(
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">sale </label>

                    <div class="col-md-8">
                        <p><input id="sale" type="text" name="sale"  onChange={handelSale}/>%</p>
                    </div>
                </div>
            )
        }else if(status == 0){
            return ("")
        }
    }

    function handelSubmit(e) {
        e.preventDefault();
        let flag = true;
        let errSubmit = {};
        if(addProduct.name.trim() == ""){
            errSubmit.name = "vui lòng nhập name";
            flag = false;
        }
        if(addProduct.price.trim() == ""){
            errSubmit.price = "Vui lòng nhập price";
            flag = false;
        }
        if(category == ""){
            errSubmit.category = "vui lòng chọn category";
            flag = false;
        }
        if(brand ==""){
            errSubmit.brand = "Vui lòng chọn brand";
            flag = false;
        }
        if(addProduct.company.trim() == ""){
            errSubmit.company = "vui lòng nhập company";
            flag = false;
        }
        if(status == ""){
            errSubmit.status = "Vui lòng chọn status";
            flag = false;
        }else if(status > 0 ){
            if(sale.trim() == ""){
                errSubmit.sale = "Vui lòng nhập sale";
                flag = false;
            }
        }
        
        if(img == ""){
            errSubmit.img = "Vui lòng chọn file ảnh";
            flag = false;
        }else{
            if(Object.keys(img).length > 3){
                errSubmit.img = "Không được vượt quá 3 file ảnh";
                flag = false;
            }else{
                Object.keys(img).map((key,index)=>{
                    const typeFile = img[key].type
                    const sizeFile = img[key].size
                    const TypeImg = typeFile.split('/').pop();
                    console.log(TypeImg);
                    
                    if(!arrTypeImage.includes(TypeImg)){
                        errSubmit.img = "file bạn chọn không phải ảnh" + typeFile;
                        flag = false;
                        console.log("xxxx" + typeFile);
                    }else{
                        if(sizeFile > (1024 * 1024)){
                            errSubmit.img = "file bạn chọn dung lượng quá 1gb";
                            flag = false;
                        }
                    }
                })
            }
            
        }
        if(addProduct.detail.trim() == ""){
            errSubmit.detail = "Vui lòng nhập detail";
            flag = false;
        }

        if(!flag){
            setErr(errSubmit);
        }else{
            let checkUser = localStorage.getItem("user");
            if(!checkUser){
                alert("Vui lòng đăng nhập");
            }else{
                checkUser = JSON.parse(checkUser)
                const accessToken = checkUser.data.success.token;
                let config = { 
                        headers: { 
                        'Authorization': 'Bearer '+ accessToken,
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Accept': 'application/json'
                        } 
                    };
                
                let formData = new FormData();
                formData.append('name',addProduct.name);
                formData.append('price',addProduct.price);
                formData.append('category',category);
                formData.append('brand',brand);
                formData.append('company',addProduct.company);
                formData.append('detail',addProduct.detail);
                formData.append('status',status);
                formData.append('sale',status==1?sale:"");
                Object.keys(img).map((item, i)=>{
                    formData.append('file[]',img[item])
                })
                 axios.post("http://demo-laravel:8888/api/user/add-product",formData,config)
                 .then(res =>{
                    if(res.data.errors){
                        console.log(res.data.errors);
                    }else{
                        console.log(res);
                    }
                 })
                 setErr("")
                 alert("thanh công")
            }
        }

    }
    return(
        <div className="col-sm-9">
            <h4>Add Product</h4>
            <form onSubmit={handelSubmit}>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">Name </label>

                    <div className="col-md-8">
                        <input id="name" type="text" name="name" onChange={handelInput} />
                    </div>
            </div>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">Price </label>

                    <div className="col-md-8">
                        <input id="price" type="text" name="price" onChange={handelInput} />
                    </div>
            </div>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">Please choose category</label>

                    <div className="col-md-8">
                    <select id="category" type="text" name="category" onChange={handelCategory}>
                        <option value="">Chọn category</option>
                        {readerCategory()}
                    </select>
                    </div>
            </div>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">Please choose brand</label>

                    <div className="col-md-8">
                        <select id="brand" type="text" name="brand" onChange={handelBrand}>
                            <option value="">Chọn brand</option>
                            {readerBrand()}
                        </select>
                    </div>
            </div>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">status</label>

                    <div className="col-md-8">
                        <select id="status" type="text" name="status" onChange={handelStatus}>
                            <option value="">chọn</option>
                            <option value="0">New</option>
                            <option value="1">Sale</option>
                        </select>
                    </div>
            </div>
            
            {rendersale()}

            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">company </label>

                    <div className="col-md-8">
                        <input id="company" type="text" name="company"  onChange={handelInput}/>
                    </div>
            </div>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">Image </label>

                    <div className="col-md-8">
                        <input id="Image" type="file" name="Image" multiple onChange={handelFile}/>
                    </div>
            </div>
            <div className="form-group row">
                    <label className="col-md-4 col-form-label text-md-right">detail </label>

                    <div className="col-md-8">
                        <textarea id="detail" type="file" name="detail" rows="11" onChange={handelInput}></textarea>
                    </div>
            </div>
            <button type="submit">Add Product</button>
            </form>
            <Errors err = {err}/>
        </div>
    )
}
export default AddProduct;