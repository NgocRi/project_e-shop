import axios from 'axios';
import { useEffect, useState } from 'react';
import {  useNavigate, useParams } from 'react-router-dom';
import StarRatings from 'react-star-ratings';

function Rate(props){
    
    const check = localStorage.getItem("check");
    let checkUser = localStorage.getItem("user");
    const params = useParams();
    const navigate = useNavigate()
    const [rating, setRating] = useState(0)

        // hiển thị tổng đánh giá
        const [totalRating, setTotalRating] = useState("")
        useEffect(()=>{
            axios.get("http://demo-laravel:8888/api/blog/rate/" + params.id)
            .then(res => {
                setTotalRating(res.data.data)
                console.log(totalRating);
            })
            .catch(err => console.log(err))
        },[])
            var a = 0; 
            var b = 0;
            
                Object.keys(totalRating).map((key,index)=>{
                    a = Number(totalRating[key]["rate"]) + a 
                    b = a /Object.keys(totalRating).length;
                })
        
        // Đánh giá 
        checkUser = JSON.parse(checkUser);
        function changeRating( newRating, name ) {
                setRating(newRating);
                if(!check){
                    alert("Vui lòng đăng nhập");
                    navigate('/Login')
                }else{
                    if(Object.keys(checkUser).length>0){
                        const accessToken = checkUser.data.success.token;
                        const id_user =  checkUser.data.Auth.id;
                        const config = { 
                            headers: { 
                            'Authorization': 'Bearer '+ accessToken,
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'Accept': 'application/json'
                            } 
                         };
                        
                        const data = {
                            "user_id": id_user,
                            "blog_id": params.id,
                            "rate": newRating
                        }
                        axios.post("http://demo-laravel:8888/api/blog/rate/" + params.id, data,config)
                        .then(res => {
                            // console.log(res.data.errosr);
                            // console.log(res);
                            
                        })
                        .catch(err => console.log(err))
                    };
                };
            };
            
    return(
        <div className="rating-area">
            <ul className="ratings">
                <li className="rate-this">Rate this item:</li>
                <li>
                    <StarRatings
                    rating={!changeRating?rating:b}
                    starRatedColor="blue"
                    changeRating={changeRating}
                    numberOfStars={6}
                    name='rating'
                />
                </li>
                <li className="color">(6 votes)</li>
            </ul>
            <ul className="tag">
                <li>TAG:</li>
                <li><a className="color" href="">Pink <span>/</span></a></li>
                <li><a className="color" href="">T-Shirt <span>/</span></a></li>
                <li><a className="color" href="">Girls</a></li>
            </ul>
        </div>
    )
}
export default Rate;