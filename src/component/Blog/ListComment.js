import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Comment from "./Comment";

function ListComment(props){

        function handelReplyComment(e){
                props.getId(e.target.id);
                // console.log(e.target.id);
        }
        
        function renderComment() {
                if(props.listComment.length > 0){
                        return props.listComment.map((value, key)=>{
                            const srcImg1 = "http://demo-laravel:8888/upload/user/avatar/" + value.image_user;
                            const nameUser1 = value.name_user
                            const comment1 = value.comment
                            const id = value.id;
                            if(value.id_comment == 0){
                                return(
                                    <ul key ={key} className="media-list">
                                                <li className="media">
                                                    
                                                    <a className="pull-left" href="#">
                                                        <img className="media-object" width="100px" src={srcImg1} alt=""/>
                                                    </a>
                                                    <div className="media-body">
                                                        <ul className="sinlge-post-meta">
                                                            <li><i className="fa fa-user"></i>{nameUser1}</li>
                                                            <li><i className="fa fa-clock-o"></i> 1:33 pm</li>
                                                            <li><i className="fa fa-calendar"></i> DEC 5, 2013</li>
                                                        </ul>
                                                        <p>{comment1}</p>
                                                        <a className="btn btn-primary"  id={id} onClick={handelReplyComment} ><i className="fa fa-reply"></i>Replay</a>
                                                    </div>
                                                </li>
                                                
                                        {/* show Comment con */}
                                        {props.listComment.map((value, index) => {
                                            const srcImg2 = "http://demo-laravel:8888/upload/user/avatar/" + value.image_user;
                                            const nameUser2 = value.name_user
                                            const comment2 = value.comment
                                            const idComment = value.id_comment
                                            if(id == idComment) {
                                                return (
                                                    <li key={key} className="media second-media">
                                                        <a className="pull-left" href="#">
                                                            <img class="media-object" width="100px" src={srcImg2} alt=""/>
                                                        </a>
                                                        <div className="media-body">
                                                            <ul className="sinlge-post-meta">
                                                                <li><i className="fa fa-user"></i>{nameUser2}</li>
                                                                <li><i className="fa fa-clock-o"></i> 1:33 pm</li>
                                                                <li><i className="fa fa-calendar"></i> DEC 5, 2013</li>
                                                            </ul>
                                                            <p>{comment2}</p>
                                                        </div>
                                                    </li>
                                                );
                                            };
                                        })};
                                    </ul>			
                                );
                            };
                        });
                };
        };

        return(
            <div class="response-area">
                    <h2>{props.listComment.length} RESPONSES</h2>
                    {renderComment()}
            </div>
        );
};
export default ListComment;