import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
function Blog(){
    const [data, setBlog] = useState("");
    
    useEffect(()=>{
        axios.get("http://demo-laravel:8888/api/blog/")
        .then(res => setBlog(res.data.blog))
        .catch(err => console.log(err)

        );
    },[]);
    function renderObj() {
        
        if(Object.keys(data).length>0){
                
                return  data.data.map((value, key)=>{
                    
                    const srcImage="http://demo-laravel:8888/upload/blog/image/" + value.image;
                    
                    return (
                                    <div key={key} className="single-blog-post">
                                        <h3>{value.title}</h3>
                                        <div className="post-meta">
                                            <ul>
                                                <li><i className="fa fa-user"></i> Mac Doe</li>
                                                <li><i className="fa fa-clock-o"></i> 1:33 pm</li>
                                                <li><i className="fa fa-calendar"></i> DEC 5, 2013</li>
                                            </ul>
                                            <span>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star"></i>
                                                <i className="fa fa-star-half-o"></i>
                                            </span>
                                        </div>
                                        <a href="">
                                            <img src= {srcImage} alt=""/>
                                        </a>
                                        <p>{value.description}</p>
                                        <Link  className="btn btn-primary" to={"/blog/detail/" + value.id}>Read More</Link>
                                    </div>
                        );
                    });

        };
    };
    return(
        <div className="col-sm-9 ">
            <div className="blog-post-area">
                <h2 className="title text-center">Latest From our Blog</h2>
                {renderObj()}
              </div>
           
            <div className="pagination-area">
                <ul className="pagination">
                    <li><a href="" className="active">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href=""><i className="fa fa-angle-double-right"></i></a></li>
                </ul>
            </div>
       </div>
    );
    
};
export default Blog;