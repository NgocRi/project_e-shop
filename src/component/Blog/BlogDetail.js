import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Comment from "./Comment";
import ListComment from "./ListComment";
import MenuLeft from "../layout/MenuLeft";
import Rate from "./Rate";

function BlogDetail(props) {
    let params = useParams();
    const [data, setBlogDetail] = useState("");
    const [listComment, setListComment] = useState([]);
    const [idREPLY, setidREPLY] = useState("");
    useEffect(()=>{
        axios.get("http://demo-laravel:8888/api/blog/detail/" + params.id)
        .then(res => {
            setBlogDetail(res.data.data)
            setListComment(res.data.data.comment)
        })
        .catch(err => console.log(err)

        );
    },[]);
    // 
    function getId(id){
        setidREPLY(id)
    }
    // 
    function getComment(data) {
        console.log(data);
        let concatArray = listComment.concat(data.data.data)
        setListComment(concatArray)
    }
    function renderBlogDetail() {
        if(Object.keys(data).length>0){
            const srcImage="http://demo-laravel:8888/upload/blog/image/" + data.image;
            
            return (
                <div  className="single-blog-post">
                                    <h3>{data.title}</h3>
                                    <div class="post-meta">
                                    
                                        <ul>
                                            <li><i className="fa fa-user"></i> Mac Doe</li>
                                            <li><i className="fa fa-clock-o"></i> 1:33 pm</li>
                                            <li><i className="fa fa-calendar"></i> DEC 5, 2013</li>
                                        </ul>
                                        <span>
                                            <i className="fa fa-star"></i>
                                            <i className="fa fa-star"></i>
                                            <i className="fa fa-star"></i>
                                            <i className="fa fa-star"></i>
                                            <i className="fa fa-star-half-o"></i>
                                        </span> 
                                    </div>
                                    <a href="">
                                        <img src={srcImage} alt=""/>
                                    </a>
                                    <p>{data.description}</p> <br/>
                                    <p>{data.content}</p> 

                                    <div className="pager-area">
                                        <ul className="pager pull-right">
                                            <li><a href="#">Pre</a></li>
                                            <li><a href="#">Next</a></li>
                                        </ul>
                                    </div>
                                </div>
                           
                    )
        }
    };
    return(
        <div  className="col-sm-9">
                <div className="blog-post-area">
                    <h2 className="title text-center">Latest From our Blog</h2>
                    { renderBlogDetail()}
                </div>
                    <Rate/>
                    <div className="socials-share">
                        <a href=""><img src="images/blog/socials.png" alt=""/></a>
                    </div>
                    

                    <div className="media commnets">
                            <a className="pull-left" href="#">
                                <img class="media-object" src="images/blog/man-one.jpg" alt=""/>
                            </a>
                            <div className="media-body">
                                    <h4 className="media-heading">Annie Davis</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <div className="blog-socials">
                                        <ul>
                                            <li><a href=""><i className="fa fa-facebook"></i></a></li>
                                            <li><a href=""><i className="fa fa-twitter"></i></a></li>
                                            <li><a href=""><i className="fa fa-dribbble"></i></a></li>
                                            <li><a href=""><i className="fa fa-google-plus"></i></a></li>
                                        </ul>
                                        <a className="btn btn-primary" href="">Other Posts</a>
                                    </div>
                            </div>
                    </div> 
                    <ListComment listComment = {listComment} getId = {getId}/>
                    <Comment getComment={getComment} idREPLY = {idREPLY}/>
        </div>    
           
    )
}
export default BlogDetail;