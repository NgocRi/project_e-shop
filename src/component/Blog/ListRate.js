import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import StarRatings from 'react-star-ratings';
import Rate from "./Rate";

function ListRate(props) {
    const [totalRating, setTotalRating] = useState("")
    const params = useParams()
    useEffect(()=>{
        axios.get("http://demo-laravel:8888/api/blog/rate/" + params.id)
        .then(res => {
            setTotalRating(res.data.data)
            console.log(totalRating);
        })
        .catch(err => console.log(err))
    },[])
    function aa() {
        var a = 0; 
        var b = 0;
        
            Object.keys(totalRating).map((key,index)=>{
                a = Number(totalRating[key]["rate"]) + a 
                b = a /Object.keys(totalRating).length;
            })
        return(
            <StarRatings
                    rating={b}
                    starRatedColor="yellow"
                    numberOfStars={6}
                    name='rating'
                />
        )
    }
    return(
        aa()
    )
    }
export default ListRate;