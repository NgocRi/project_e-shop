import axios from "axios";
import { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

function Comment(props){
    const [comment, setComment] = useState("");
    const navigate = useNavigate();
    let params = useParams();
    
    const checkLogin = localStorage.getItem("check");
    const check = localStorage.getItem("user");
    let obj = {}
    obj = JSON.parse(check);
    
    function handelTextarea(e){
        setComment(e.target.value)
    }
    function handelSubmit(e){
        e.preventDefault();
       
        if(checkLogin){
            if(comment.trim() == ""){
                alert("ko đc để trống")
            }else{
                let url = "http://demo-laravel:8888/api/blog/comment/" + params.id;
                let config,accessToken,id_user,avatar,name_user;
                if(Object.keys(obj).length > 0){
                    accessToken = obj.data.success.token;
                    id_user =  obj.data.Auth.id;
                    avatar = obj.data.Auth.avatar;
                    name_user= obj.data.Auth.name;
                    config = { 
                        headers: { 
                        'Authorization': 'Bearer '+ accessToken,
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Accept': 'application/json'
                        } 
                     };
                }
                const formData = new FormData();
                formData.append('id_blog', params.id);
                formData.append('id_user',id_user);
                formData.append('id_comment',props.idREPLY ? props.idREPLY :  0);
                formData.append('comment',comment);
                formData.append('image_user',avatar);
                formData.append('name_user',name_user);
                axios.post(url, formData, config)
                .then(res =>{
                    if(res.data.errors){
                        console.log(res.data.errors);
                        console.log(res);
                    }
                    // truyền lên cha
                    props.getComment(res)
                })
                .catch( 
                    err => console.log(err)
                )
            }
        }else{
            alert("Vui lòng đăng nhập");
            navigate('/Login');
            
        }
    }
    return(
        <section>
            <div className="text-area">
                <div className="blank-arrow">
                    <label>Your Name</label>
                </div>
                <span>*</span>
                <textarea name="message" rows="11" onChange={handelTextarea}></textarea>
                <a className="btn btn-primary" onClick={handelSubmit}  >post comment</a>
            </div>
        </section>
    )
}
export default Comment;