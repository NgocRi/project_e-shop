import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './component/Product/Home';
import Login from './component/Account/Login';
import Register from './component/Account/Register';
import Blog from './component/Blog/Blog';
import BlogDetail from './component/Blog/BlogDetail';
import Checkout from './component/Checkout';
import Contact from './component/Contact';
import Account from './component/Account/Account';
import MyProduct from './component/Product/MyProduct';
import AddProduct from './component/Product/AddProduct';
import EditProduct from './component/Product/EditProduct';
import ProductDetail from './component/Product/ProductDetail';
import ListProduct from './component/Product/ListProduct';
import Cart from './component/Product/Cart';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Router>
      <App>
        <Routes>
          <Route exact path='/' element={<Home/>}/>npm st
          <Route  path='/Login' element={<Login/>}/>
          <Route  path='/Register' element={<Register/>}/>
          <Route  path='/Blog' element={<Blog/>}/>
          <Route  path='/blog/detail/:id' element={<BlogDetail/>}/>
          <Route  path='/Checkout' element={<Checkout/>}/>
          <Route  path='/Cart' element={<Cart/>}/>
          <Route  path='/Contact' element={<Contact/>}/>
          <Route  path='/Account' element={<Account/>}/>
          <Route  path='/MyProduct' element={<MyProduct/>}/>
          <Route  path='/AddProduct' element={<AddProduct/>}/>
          <Route  path='/EditProduct/:id' element={<EditProduct/>}/>
          <Route  path='/ProductDetail/:id' element={<ProductDetail/>}/>
          <Route  path='/ListProduct' element={<ListProduct/>}/>
        </Routes>
      </App>
    </Router>
    
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
